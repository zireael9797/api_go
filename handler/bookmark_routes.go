package handler

import (
	"context"
	"fmt"
	"log"
	"main/model"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func AddBookmark() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("ADDBOOKMARK: called")
		//--------------DB Connection
		gamecollection := c.MustGet("games").(*mongo.Collection)
		//--------------DB Connection
		collection := c.MustGet("bookmarks").(*mongo.Collection)

		articleId, _ := primitive.ObjectIDFromHex(c.Param("aid"))
		userId, _ := primitive.ObjectIDFromHex(c.MustGet("userId").(string))

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		gameres := gamecollection.FindOne(ctx, bson.M{"_id": articleId})

		if gameres.Err() != nil {
			fmt.Println("Aborting Bookmark")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Game doesn't exist"})
		} else {
			fmt.Println("Adding Bookmark")
			res, err := collection.InsertOne(ctx, model.Bookmark{
				ArticleId: articleId,
				UserId:    userId,
			})
			if err != nil {
				log.Fatal(err)
				c.JSON(http.StatusInternalServerError, err)
			} else {
				fmt.Println(res)
				c.JSON(http.StatusCreated, res)
			}
		}

	}
}
