package handler

import (
	"context"
	"log"
	"net/http"
	"time"

	"main/model"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetGames() gin.HandlerFunc {
	return func(c *gin.Context) {
		//--------------DB Connection
		collection := c.MustGet("games").(*mongo.Collection)

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

		cursor, err := collection.Find(ctx, bson.M{})
		if err != nil {
			log.Fatal(err)
		}
		results := make([]model.Game, 0)
		for cursor.Next(ctx) {
			var result model.Game
			err := cursor.Decode(&result)
			if err != nil {
				log.Fatal(err)
			}
			results = append(results, result)

		}
		c.JSON(200, gin.H{"count": len(results), "articles": results})
	}
}

func GetGameById() gin.HandlerFunc {
	return func(c *gin.Context) {

		//--------------DB Connection
		collection := c.MustGet("games").(*mongo.Collection)

		id, _ := primitive.ObjectIDFromHex(c.Param("aid"))

		article := model.Game{}

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		err := collection.FindOne(ctx, model.Game{Id: id}).Decode(&article)
		if err != nil {
			log.Fatal(err)
			c.JSON(http.StatusNotFound, gin.H{
				"error": err,
			})
		}
		c.JSON(http.StatusOK, gin.H{"article": article})
	}
}
