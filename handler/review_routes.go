package handler

import (
	"context"
	"fmt"
	"log"
	"main/model"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func GetReviews() gin.HandlerFunc {
	return func(c *gin.Context) {

		//--------------DB Connection
		collection := c.MustGet("reviews").(*mongo.Collection)

		articleId, _ := primitive.ObjectIDFromHex(c.Param("aid"))

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		cursor, err := collection.Find(ctx, model.Review{ArticleId: articleId})
		if err != nil {
			log.Fatal(err)
		}
		results := make([]model.Review, 0)
		for cursor.Next(ctx) {
			var result model.Review
			err := cursor.Decode(&result)
			if err != nil {
				log.Fatal(err)
			}
			results = append(results, result)

		}
		c.JSON(http.StatusOK, gin.H{"count": len(results), "comments": results})
	}
}

func PostReview() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("POSTCOMMENT: called")

		//--------------DB Connection
		gamecollection := c.MustGet("games").(*mongo.Collection)
		//--------------DB Connection
		collection := c.MustGet("reviews").(*mongo.Collection)

		articleId, _ := primitive.ObjectIDFromHex(c.Param("aid"))
		userId, _ := primitive.ObjectIDFromHex(c.MustGet("userId").(string))
		username, _ := c.MustGet("username").(string)
		email, _ := c.MustGet("email").(string)

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		gameres := gamecollection.FindOne(ctx, bson.M{"_id": articleId})

		fmt.Println("PostComment: Username is: ", username)
		if gameres.Err() != nil {
			fmt.Println("Aborting Review")
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Game doesn't exist"})
		} else {
			fmt.Println("Adding Review")
			res, err := collection.InsertOne(ctx, model.Review{
				ArticleId: articleId,
				Comment:   c.PostForm("comment"),
				Username:  username,
				UserId:    userId,
				Email:     email,
			})
			if err != nil {
				log.Fatal(err)
				c.JSON(http.StatusInternalServerError, err)
			} else {
				fmt.Println(res)
				c.JSON(http.StatusCreated, res)
			}
		}
	}
}

func DeleteReview() gin.HandlerFunc {
	return func(c *gin.Context) {

		articleId, _ := primitive.ObjectIDFromHex(c.Param("aid"))
		commentId, _ := primitive.ObjectIDFromHex(c.Param("cid"))
		userId, _ := primitive.ObjectIDFromHex(c.MustGet("userId").(string))

		//--------------DB Connection
		collection := c.MustGet("reviews").(*mongo.Collection)

		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		res, err := collection.DeleteOne(ctx, model.Review{ArticleId: articleId, Id: commentId, UserId: userId})
		if err != nil {
			log.Fatal(err)
			c.JSON(http.StatusInternalServerError, err)
		} else {
			fmt.Println("DELETED_COMMENT: ")
			c.JSON(http.StatusOK, res)
		}
	}
}
