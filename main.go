package main

import (
	"main/database"
	"main/handler"
	"main/middleware"

	"github.com/gin-gonic/gin"
)

func main() {
	database.Database()

	router := gin.Default()

	router.Use(middleware.CORSMiddleware())
	router.Use(middleware.DatabaseConnection(database.DB))

	//--------------------------Basic Routes
	router.GET("/articles", handler.GetGames())
	router.GET("/articles/:aid", handler.GetGameById())
	router.GET("/articles/:aid/comments", handler.GetReviews())

	//--------------------------Authentication Required Routes
	authorized := router.Group("/")
	authorized.Use(middleware.CheckAuthorization())
	{
		authorized.POST("/articles/:aid/comments", handler.PostReview())
		authorized.DELETE("/articles/:aid/comments/:cid", handler.DeleteReview())
		//auhtorized.PUT("/articles/:aid/comments/:cid", handler.UpdateReview())

		authorized.POST("user/bookmarks/games/:aid", handler.AddBookmark())
	}

	//--------------------------User Routes
	router.POST("/users/signup", handler.Signup())
	router.POST("/users/login", handler.Login())
	router.POST("/users/sudo", handler.Sudo())

	router.Run(":3693")
}

//env GOOS=linux GOARCH=arm go build -o api_arm
