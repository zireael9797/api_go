package middleware

import (
	"context"
	"fmt"
	"main/model"
	"net/http"
	"time"

	"github.com/gbrlsnchs/jwt/v3"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var hs = jwt.NewHS256([]byte("secret"))

func CheckAuthorization() gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println("AUTH_CHECK: called")
		token := []byte(c.GetHeader("token"))
		fmt.Print(string(token))
		if len(token) > 0 {
			//--------------------------------IF TOKEN PROVIDED
			var pl model.CustomPayload
			_, err := jwt.Verify(token, hs, &pl)
			if err == nil {

				//--------------DB Connection
				collection := c.MustGet("users").(*mongo.Collection)

				ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
				userId, _ := primitive.ObjectIDFromHex(pl.Subject)
				userres := collection.FindOne(ctx, bson.M{"_id": userId})
				if userres.Err() != nil {
					//--------------------------------IF USER DOESN'T EXIST
					fmt.Println("Invalid User")
					c.JSON(http.StatusInternalServerError, gin.H{"error": "User doesn't exist"})
					c.Abort()
					return
				} else {
					user := model.User{}
					err := userres.Decode(&user)
					if err != nil {
						c.JSON(http.StatusInternalServerError, gin.H{"error": "internal error"})
						c.Abort()
						return
					} else {
						c.Set("userId", pl.Subject)
						c.Set("username", user.Username)
						c.Set("email", user.Email)
						c.Next()
					}
				}
			} else {
				c.JSON(http.StatusForbidden, gin.H{
					"error": err,
				})
				c.Abort()
				return
			}
		} else {
			//--------------------------------IF TOKEN NOT PROVIDED
			c.JSON(http.StatusUnauthorized, gin.H{
				"error": "You must provide an authorization token",
			})
			c.Abort()
			return
		}
	}
}
