package middleware

import (
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

func DatabaseConnection(DB *mongo.Database) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("games", DB.Collection("articles"))
		c.Set("reviews", DB.Collection("comments"))
		c.Set("users", DB.Collection("users"))
		c.Set("bookmarks", DB.Collection("bookmarks"))
	}
}
