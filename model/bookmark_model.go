package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type Bookmark struct {
	Id        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserId    primitive.ObjectID `json:"userid,omitempty" bson:"userid,omitempty"`
	ArticleId primitive.ObjectID `json:"articleid,omitempty" bson:"articleid,omitempty" required:"true"`
}
