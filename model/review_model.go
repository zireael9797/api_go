package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type Review struct {
	Id        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	UserId    primitive.ObjectID `json:"userid,omitempty" bson:"userid,omitempty"`
	Username  string             `json:"username,omitempty" bson:"username,omitempty" required:"true"`
	Email     string             `json:"email,omitempty" bson:"email,omitempty" required:"true"`
	Comment   string             `json:"comment,omitempty" bson:"comment,omitempty" required:"true"`
	ArticleId primitive.ObjectID `json:"articleid,omitempty" bson:"articleid,omitempty" required:"true"`
}
